﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Aug_2022.model
{
    internal class Details
    {
        public int Id;
        public string Name;
        public double price;

        public Details(int Id,string Name,double price) { 
            this.Id = Id;
            this.Name = Name;   
            this.price = price;
        }
    }
}
