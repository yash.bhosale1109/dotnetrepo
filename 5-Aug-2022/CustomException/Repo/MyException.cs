﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CustomException.Repo
{

    internal class UserName {
        public string name;

        public UserName(String name) { 
            this.name = name;
        }

        public static void IsCorret(string uname) {
            string pattern = @"^[a - zA - Z]+$";
            if (Regex.IsMatch(uname, pattern)) {
                throw new MyException(uname);
            }
        }
    }

    internal class MyException:Exception
    {
        MyException() { 
        
        }
         public MyException(string uname):base() {
            Console.WriteLine($"Invalid UserName {uname}");
        }
    }
}
