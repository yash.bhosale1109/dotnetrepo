﻿// See https://aka.ms/new-console-template for more information
using CustomException.Repo;
using System.Text.RegularExpressions;

class Program
{
    static void Main(string[] args)
    {
        Student newStudent = null;

        try
        {
            newStudent = new Student();
            newStudent.StudentName = "James007";

            ValidateStudent(newStudent);
        }
        catch (InvalidStudentNameException ex)
        {
            Console.WriteLine(ex.Message);
        }


        Console.ReadKey();
    }

    private static void ValidateStudent(Student std)
    {
        Regex regex = new Regex("^[a-zA-Z]+$");

        if (!regex.IsMatch(std.StudentName))
            throw new InvalidStudentNameException(std.StudentName);

    }
}
