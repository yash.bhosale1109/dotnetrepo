﻿using _5_Aug_2022.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_Aug_2022.Repos
{
    internal class UserRegister
    {

        public bool RegisterUser(User user) {
            string filename = "userRegister.txt";
            if (user != null)
            {
                if (!IsUserExist(user.Name, filename))
                {
                    AddUser(user);
                    return true;
                }
                else
                {
                    Console.WriteLine("User Already Exist");
                    return false;
                }
            }
            return false;
        }

        public bool IsUserExist(string user,string Filename) {
            try
            {
                using (StreamReader sr = new StreamReader(Filename))
                {

                    while (sr.Peek() >= 0)
                    {
                        string[] line = sr.ReadLine().Split("/");
                        if (line[1] == user)
                        {
                            return true;
                        }

                    }
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e);
            }
            return false;


        }

        public List<string> GetAllUser() {
            List<String> list = new List<string>();
            try
            {
                string FileName = "userRegister.txt";
                
                using (StreamReader sr = new StreamReader(FileName))
                {

                    while (sr.Peek() >= 0)
                    {
                        string line = sr.ReadLine();
                        list.Add(line);
                    }
                }
                return list;
            }

            catch (FileNotFoundException e) {
                Console.WriteLine(e);
            }
            return list;
        }

        public void DeleteUser(string userName) {
            string FileName = "userRegister.txt";
            string FileName1 = "userRegister1.txt";
            using (StreamReader sr = new StreamReader(FileName))
            {
                using (StreamWriter sw = new StreamWriter(FileName1)) {

                    while (sr.Peek() >= 0)
                    {
                        string[] line = sr.ReadLine().Split("/");
                        if (int.Parse(line[0]) != 1)
                        {
                            sw.WriteLine($"{line[0]}/{line[1]}/{line[2]}"+Environment.NewLine);
                        }
                        else {
                            continue;
                        }
                    }

                }

                   
            }



        }

        private bool AddUser(User user) {
            string filename = "userRegister.txt";
            using (StreamWriter sw = new StreamWriter(filename, true)) {

                sw.Write($"{user}"+Environment.NewLine);
            }
            return true;
        }


    }
}
