﻿// See https://aka.ms/new-console-template for more information
using _5_Aug_2022.Model;
using _5_Aug_2022.Repos;

Console.WriteLine("Hello, World!");
UserRegister userRegister = new UserRegister();
//User user = new User(1,"Yash",22);
//Console.WriteLine(  userRegister.RegisterUser(user));
//user = new User(2, "chacha", 23);
//Console.WriteLine(userRegister.RegisterUser(user));

//userRegister.DeleteUser("Yash");
while (true) {

    Console.WriteLine("Enter \n1:Register User\n2:PrintUser\n3:Delete User\n-1:exit");
    int options = int.Parse(Console.ReadLine());
    if (options == -1) {
        break;
    }
    else if (options == 1)
    {
        Console.WriteLine("Enter the User Details");
        Console.WriteLine("Enter the User Id");
        int id = int.Parse(Console.ReadLine());
        Console.WriteLine("Enter the UserName");
        string? name = Console.ReadLine();
        Console.WriteLine("Enter the Age");
        int age = int.Parse(Console.ReadLine());
        User user = new User(id, name, age);
        userRegister.RegisterUser(user);
    }
    else if (options == 2)
    {
        List<string> str = userRegister.GetAllUser();

        foreach (string strItem in str)
        {
            Console.WriteLine(strItem);
        }
    }
    else if (options == 3)
    {
        string name = Console.ReadLine();
        userRegister.DeleteUser(name);
    }
    else
    {
        Console.WriteLine("Invalid Option");
    }


}
    



