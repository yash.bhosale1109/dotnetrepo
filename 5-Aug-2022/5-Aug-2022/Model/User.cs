﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_Aug_2022.Model
{
    internal class User
    {
        public int Id;
        public string Name;
        public int Age;

        public User(int Id,string Name,int Age) { 
            this.Id = Id;
            this.Name = Name;
            this.Age = Age;
        }
        public override string ToString()
        {
            return $"{Id}/{Name}/{Age}";
        }


    }
}
