﻿// See https://aka.ms/new-console-template for more information

using _4_Aug_2022.Models;
using _4_Aug_2022.Repository;

MethodsImplementation methodImplementation = new MethodsImplementation();

Console.WriteLine("Product Creating");
Product product = new Product("Mobile", 10000);
string res1 = methodImplementation.AddToCart(product);
Console.WriteLine(res1);
product = new Product("Tv", 20000);
res1 = methodImplementation.AddToCart(product);
Console.WriteLine(res1);
product = new Product("Tv", 30000);
res1 = methodImplementation.AddToCart(product);
Console.WriteLine(res1);
product = new Product("Mobile", 40000);
res1 = methodImplementation.AddToCart(product);
Console.WriteLine(res1);

List<Product> prod = methodImplementation.GetProduct();
foreach (Product p in prod)
{
    Console.WriteLine($"{p.name} \t {p.price}");
}

Console.WriteLine(".....xxxx.....");
methodImplementation.DeleteProduct();
List<Product> prod2 = methodImplementation.GetProduct();
foreach (Product p in prod2)
{
    Console.WriteLine($"{p.name} \t {p.price}");
}

//methodImplementation.GetProdcutByName("Mobile");


