﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Aug_2022.Models
{
    internal class Product
    {
        public string name;
        public int price;
        public Product(string name, int price) {
            this.name = name;
            this.price = price;
        }
    }
}
