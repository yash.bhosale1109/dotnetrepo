﻿using _4_Aug_2022.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Aug_2022.Repository
{
    internal interface IGetProduct
    {
        List<Product> GetProduct();

        void DeleteProduct();

        void GetProdcutByName(string name);
    }
}
