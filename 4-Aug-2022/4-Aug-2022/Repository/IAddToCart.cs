﻿using _4_Aug_2022.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Aug_2022.Repository
{
    interface IAddToCart
    {
        string AddToCart(Product product);
    }
}
