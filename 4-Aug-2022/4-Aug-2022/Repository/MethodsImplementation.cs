﻿using _4_Aug_2022.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Aug_2022.Repository
{
    internal class MethodsImplementation:IAddToCart,IGetProduct
    {
        List<Product> products;
        public MethodsImplementation() { 
            products = new List<Product>();
        }

        public string AddToCart(Product product)
        {
            products.Add(product);
             return "Product Added Successfully";
            
        }

        public void DeleteProduct()
        {
            if (products != null)
            {
                products.RemoveAll(x => x.name == "Tv");
            }
            else {
                Console.WriteLine("Nothing to Delete");
            }
        }

        public void GetProdcutByName(string name)
        {
            foreach (Product product in products) {
                if (product.name == name) {
                    Console.WriteLine($"{product.name} \t {product.price}");
                }
            }
        }

        //public List<Product> UpdateProduct()
        //{
        //    if (products != null) { 
               
        //    }
        //}

        public List<Product> GetProduct()
        {
            return products;
        }
    }
}
