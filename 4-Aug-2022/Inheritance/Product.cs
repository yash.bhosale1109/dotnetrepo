﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    abstract class Product
    {
        public string Name { get; set; }
        public int price { get; set; }

         public Product(string name, int price)
        {
            Name = name;
            this.price = price;
        }

        public abstract void AddtoCart(int quality);

        public abstract void GetDetails();
    }
    
}
