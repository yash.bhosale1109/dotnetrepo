﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    internal class Book : Product
    {
        public string Auther{ get; set; }

        public Book(string name,int price , string auther):base(name,price)
        {
            Auther = auther;
        }
        public override void GetDetails() {
            Console.WriteLine($"The Name of The Book {this.Name} Author is {this.Auther} Price is {this.price}");
        }

        public override void AddtoCart(int quality)
        {
            int discount = 500;
            double carttotal = (price * quality) - discount;
            Console.WriteLine(carttotal);
        }

    }
}
