﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Regularexpression
{
    internal class PasswordValidate
    {
        public const string pattern = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,15}$";

        public  bool Validate(string Password) {
            if (Password != null)
            {
                return Regex.IsMatch(Password, pattern);
            }
            else {
                return true;
            }

        }
    }
}
