﻿using _3_Aug_2022.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Aug_2022.Repo
{


    internal class MethodsDeclaration
    {
        ProductStructure[] prodarray;
        public MethodsDeclaration()
        {
            prodarray = new ProductStructure[] {
                new ProductStructure(0,"lava",12000,3.3f),
                new ProductStructure(1,"lava",12000,3.3f),
                new ProductStructure(2,"oppo",13000,3.5f),
                new ProductStructure(3,"redmi",14000,2.5f),
                new ProductStructure(4,"apple",15000,4.5f),
                new ProductStructure(5,"Samsung",16000,2.5f),
                new ProductStructure(6,"Samsung",18000,3.5f),
                new ProductStructure(7,"Samsung",18000,4.5f),
                new ProductStructure(8,"Moto",17000,4.5f),
            };

        }
        public ProductStructure[] GetALlProduct() {
            return prodarray;
        }
        public ProductStructure[] GetSpecific() {
            return prodarray;
        }
        public ProductStructure[] GetProductwithprice() {
            return prodarray;
        }
        public void GetByName(string name) {
            bool entered = false;
            foreach (ProductStructure item in prodarray) {
                if (item.prodName == name) {
                    entered = true;
                    Console.WriteLine($"Name :: {item.prodName} \t price is:: {item.prodPrice} \t rating is {item.rating}");
                }
            }
            if (!entered) {
                Console.WriteLine("No match");
            }
        }
        public ProductStructure[] GetDelete()
        {
            foreach (ProductStructure item in prodarray) {
                if (item.prodId <= 2) {
                    item.prodId = 0;
                    item.prodName = "";
                    item.prodPrice = 0;
                    item.rating = 0;

                }
            }
            return prodarray;
        }

        public ProductStructure[] GetUpdateData(string productname, int val) {

            foreach (ProductStructure item in prodarray) {
                if (item.prodName == null) {
                    continue;
                }
                if (item.prodName == productname) {
                    item.prodPrice -= val;
                }
            }
            return prodarray;

        }
        public void Deletevalues(int i) {
            if (i >= 0 && i < prodarray.Length)
            {
                while (i < prodarray.Length - 1)
                {
                    prodarray[i] = prodarray[i + 1];
                    i++;
                }
                prodarray[prodarray.Length - 1] = null;
            }
            else {
                Console.WriteLine("Invalid Index");
            }
            //for (int j = 0; j < prodarray.Length; j++) {
            //    if (j == i) {
            //        prodarray[j] = null;
            //    }
            //    //Console.WriteLine(string.Join(",", prodarray[j]));
            //}
        }
        
    }
}
