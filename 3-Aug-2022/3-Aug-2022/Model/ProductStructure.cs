﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Aug_2022.Model
{
    internal class ProductStructure
    {
        public int prodId;
        public string prodName;
        public int prodPrice;
        public float rating;

        public ProductStructure(int prodId, string prodName, int prodPrice, float rating)
        {
            this.prodId = prodId;
            this.prodName = prodName;
            this.prodPrice = prodPrice;
            this.rating = rating;
        }

        public override string ToString()
        {
            return $"id : {prodId} \t name :: {prodName} \t     price:: {prodPrice} \t rating : {rating}";
        }
    }
}
